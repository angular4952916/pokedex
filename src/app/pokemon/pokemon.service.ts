import { Injectable } from "@angular/core";
import { Pokemon } from "./pokemon.models";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, catchError, map, of, tap } from "rxjs";
import { AuthService } from "../auth.service";

@Injectable({
  providedIn: "root",
})
export class PokemonService {
  private apiBaseUrl = 'http://localhost:3000';


  private getHeaders(): HttpHeaders {
    const token = this.authService.getToken();
    let headers = new HttpHeaders({
      'Content-type': 'application/json'
     });

     if(token) {
      headers = headers.set('Authorization', `Bearer: ${token}`)
     }

    return headers;
  }

  private prepareOptions(): {headers: HttpHeaders} {
    return {headers: this.getHeaders()};
  }

  constructor(private http: HttpClient, private authService: AuthService) {}

  createPokemon(newPokemon: Pokemon|undefined): Observable<Pokemon | undefined> {

    return this.http.post<Pokemon>(`${this.apiBaseUrl}/api/pokemons`, newPokemon, this.prepareOptions()).pipe(
      tap((response) => console.table(response)),
      catchError((error) => {
        console.log(error);
        return of(undefined); // Retourne undefined en cas d'erreur
      })
    );
  }

  updatepokemon(updatedPokemon: Pokemon|undefined): Observable<null|undefined> {

    return this.http.put<null> (`${this.apiBaseUrl}/api/pokemons/${updatedPokemon?._id}`, updatedPokemon , this.prepareOptions()).pipe(
      tap((response) => console.table(response)),
      catchError((error) => {
        console.log(error);
        return of(undefined);
      }))
  }

  getTypesList(): string[] {
    return [
      "Feu",
      "Eau",
      "Plante",
      "Insecte",
      "Normal",
      "Vol",
      "Poison",
      "Fée",
      "Psy",
      "Electrik",
      "Combat",
    ];
  }

  getPokemonList(): Observable<Pokemon[]> {
    console.log("Appel de getPokemonList() depuis pokemonService");

    return this.http.get<Pokemon[]> (`${this.apiBaseUrl}/api/pokemons`, this.prepareOptions()).pipe(
      tap((response) => {
        console.table(response)
      }),
      catchError((error) => {
        console.log('Error ...')
        console.log(error);
        return of([]);
      }))
  }

  searchPokemonList(term: string): Observable<Pokemon[]> {
    console.log(`Recherche de Pokémon avec le terme: ${term}`);
    
    return this.http.get<Pokemon[]>(`${this.apiBaseUrl}/api/pokemons/?name=${term}`, this.prepareOptions()).pipe(
      tap((response) => {
        console.table(response)
      }),
      catchError((error) => {
        console.log('Error ...')
        console.log(error);
        return of([]);
      }))
  }



  // updatePokemonList(pokemon: Pokemon | undefined) {
  //   if (pokemon != undefined) {
  //     // Mettez à jour le Pokémon dans la liste
  //     const index = POKEMONS.findIndex((p) => p.id === pokemon.id);
  //     if (index !== -1) {
  //       POKEMONS[index] = pokemon;
  //       // Mettez à jour le localStorage
  //       localStorage.setItem("pokemons", JSON.stringify(POKEMONS));
  //     }
  //   }
  // }

 
  getPokemonById(pokemonId: string): Observable<Pokemon|undefined> {
    return this.http.get<Pokemon|undefined> (`${this.apiBaseUrl}/api/pokemons/${pokemonId}`, this.prepareOptions()).pipe(
      tap((response) => console.log(response)),
      catchError((error) => {
        console.log(error);
        return of(undefined);
      }))
  }

  getPokemonsFromLocalStorage(): Pokemon[] {
    const storedPokemons = localStorage.getItem('pokemons');
    if (storedPokemons) {
      try {
        return JSON.parse(storedPokemons);
      } catch (error) {
        console.error('Error parsing JSON from local storage:', error);
        return [];
      }
    } else {
      return [];
    }
  }
  
}
