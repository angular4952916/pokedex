import { Component } from '@angular/core';
import { Pokemon } from '../pokemon.models';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from '../pokemon.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: [
    './detail-pokemon.component.css'
  ],
})
export class DetailPokemonComponent {
  pokemonList: Pokemon[] = [];
  pokemon: Pokemon|undefined;
  private pokemonListSubscription: Subscription | undefined;



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public pokemonService: PokemonService,
  ) {}

  ngOnInit(){
    this.pokemonListSubscription = this.pokemonService.getPokemonList()
      .subscribe(pokemons => {
        this.pokemonList = pokemons;        
  
        const pokemonId: string | null = this.route.snapshot.paramMap.get('id');
        
        if(pokemonId) {
          this.pokemon = this.pokemonList.find(pokemon => pokemon._id == pokemonId );
          console.log('Pokémon sélectionné:', this.pokemon);
        }
      })
  }

  ngOnDestroy() {
    // Assurez-vous de vous désabonner pour éviter les fuites de mémoire
    if (this.pokemonListSubscription) {
      this.pokemonListSubscription.unsubscribe();
    }
  }

  goBack(): void {
    this.router.navigate(['pokemons'], { replaceUrl: true });
  }

  goToEditPage(id: string): void {
    this.router.navigate(['edit/pokemon', id], { replaceUrl: true });
  }
}
