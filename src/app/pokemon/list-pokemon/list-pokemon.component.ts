import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "../pokemon.models";
import { PokemonService } from "../pokemon.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-list-pokemon",
  templateUrl: "./list-pokemon.component.html",
  styles: [],
})
export class ListPokemonComponent {
  pokemonList: Pokemon[] = [];
  pokemonSelected: Pokemon | undefined;
  pokemonSearchTerm = "";
  pokemonsFromLocalStorage: Pokemon[] = [];
  private pokemonListSubscription: Subscription | undefined;


  constructor(private router: Router, public pokemonService: PokemonService) {}

  ngOnInit() {
    console.log("Init ListPokemonComponent");
    
    this.pokemonListSubscription = this.pokemonService.getPokemonList()
    .subscribe(pokemons => this.pokemonList = pokemons)
    console.log(this.pokemonList);
  }

  ngOnDestroy() {
    // On s'assure de se désabonner pour éviter les fuites de mémoire
    if (this.pokemonListSubscription) {
      this.pokemonListSubscription.unsubscribe();
    }
  }

  get filteredPokemonList(): Pokemon[] {
    return this.pokemonList
      .filter((pokemon) =>
        pokemon.name.toLowerCase().includes(this.pokemonSearchTerm.toLowerCase())
      )
      .sort((a, b) => {
        // Trie les Pokémon par date de création, du plus récent au plus ancien
        return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
      });
  }
  

  selectPokemon(pokemonId: string): void {

    // this.pokemonService.getPokemonById(+pokemonId).subscribe(
    //   pokemon => this.selectPokemon = pokemon
    // );
  }

  viewPokemonDetail(pokemonId: string) {
    this.router.navigate(["/pokemon", pokemonId], { replaceUrl: true });
  }

  addPokemon(): void {
    this.router.navigate(["/add/pokemon"], { replaceUrl: true });
  }
}
