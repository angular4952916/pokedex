import { trigger, transition, style, query, animateChild, group, animate } from '@angular/animations';

export const slideInAnimation =
  trigger('routeAnimations', [
    transition('pokemonList => pokemonDetail, pokemonDetail => pokemonList', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '-100%' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('300ms ease-out', style({ left: '100%' }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ left: '0%' }))
        ])
      ]),
      query(':enter', animateChild())
    ])
  ]);


export const fadeAndSlideAnimation =
  trigger('routeAnimations', [
    transition('pokemonList => pokemonDetail', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ opacity: 0, transform: 'translateY(-100%)' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('300ms ease-out', style({ opacity: 0, transform: 'translateY(100%)' }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ opacity: 1, transform: 'translateY(0%)' }))
        ])
      ]),
      query(':enter', animateChild())
    ]),
    transition('pokemonDetail => pokemonList', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ opacity: 0, transform: 'translateY(100%)' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('300ms ease-out', style({ opacity: 0, transform: 'translateY(-100%)' }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ opacity: 1, transform: 'translateY(0%)' }))
        ])
      ]),
      query(':enter', animateChild())
    ])
  ]);


export const flipAnimation =
  trigger('routeAnimations', [
    transition('pokemonList => pokemonDetail', [
      style({ height: '!' }),
      query(':enter', [
        style({ transform: 'perspective(400px) rotateY(-90deg)', opacity: 0 })
      ]),
      query(':leave', [
        style({ transform: 'perspective(400px) rotateY(0deg)', opacity: 1 }),
        animate('600ms', style({ transform: 'perspective(400px) rotateY(90deg)', opacity: 0 }))
      ]),
      query(':enter', [
        style({ transform: 'perspective(400px) rotateY(-90deg)', opacity: 0 }),
        animate('600ms', style({ transform: 'perspective(400px) rotateY(0deg)', opacity: 1 }))
      ]),
    ]),
    transition('pokemonDetail => pokemonList', [
      style({ height: '!' }),
      query(':enter', [
        style({ transform: 'perspective(400px) rotateY(90deg)', opacity: 0 })
      ]),
      query(':leave', [
        style({ transform: 'perspective(400px) rotateY(0deg)', opacity: 1 }),
        animate('600ms', style({ transform: 'perspective(400px) rotateY(-90deg)', opacity: 0 }))
      ]),
      query(':enter', [
        style({ transform: 'perspective(400px) rotateY(90deg)', opacity: 0 }),
        animate('600ms', style({ transform: 'perspective(400px) rotateY(0deg)', opacity: 1 }))
      ]),
    ])
  ]);



export const scaleAnimation =
  trigger('routeAnimations', [
    transition('pokemonList <=> pokemonDetail', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('300ms ease-out', style({ transform: 'scale(0.5)', opacity: 0 }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ transform: 'scale(1)', opacity: 1 }))
        ])
      ]),
      query(':enter', animateChild())
    ])
  ]);