import { Component } from '@angular/core';
import { fadeAndSlideAnimation, flipAnimation, scaleAnimation, slideInAnimation } from './animations/animations';
import { Router, RouterOutlet } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: `./app.component.html`,
  styles: [],
  animations: [flipAnimation]
})
export class AppComponent {
  isLoggedIn: boolean|undefined;
  constructor(public authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.isLoggedIn = this.isToken();
  }

  isToken (): boolean {
    const token = localStorage.getItem('token');
    console.log('Check token', token);
    
  if (token) {
    return true;
  }
  else {
    return false;
  }
  } 

  goToUserProfile(): void {
    this.router.navigate(['/user/profile'], { replaceUrl: true });
  }
  
  logout() {
    this.authService.logout();
  }
  
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
 }