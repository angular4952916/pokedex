import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { FormsModule } from '@angular/forms';


const usersRoutes: Routes = [
  {path: 'user/profile', component: ProfileComponent, canActivate:[AuthGuard]},

];


@NgModule({
  declarations: [
    ProfileComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(usersRoutes)
  ]
})
export class UserModule { }
