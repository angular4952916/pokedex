import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-profile',
  templateUrl: `./profile.component.html`,
  styles: [
  ]
})
export class ProfileComponent implements OnInit{

  userInfo: any;
  isEditable = false;
  originalUserInfo: any;



  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.getUserInfo();
  }

  getUserInfo(): void {
    this.userService.getUserInfoFromToken().subscribe(
      (userInfo) => {
        this.userInfo = userInfo;
        this.originalUserInfo = { ...userInfo }; // Sauvegarde des informations originales de l'utilisateur
      },
      (error) => {
        console.error('Une erreur est survenue lors de la récupération des informations de l\'utilisateur :', error);
      }
    );
  }

  toggleEditMode(): void {
    this.isEditable = !this.isEditable;
  }

  updateProfile(): void {
    // Envoyer les modifications au backend (à implémenter)
    console.log('Informations mises à jour :', this.userInfo);
    // Après la mise à jour, revenir à l'état non modifiable
    this.toggleEditMode();
  }

  cancelUpdate(): void {
    // Annuler les modifications et restaurer les informations originales
    this.userInfo = { ...this.originalUserInfo };
    // Revenir à l'état non modifiable
    this.toggleEditMode();
  }

  deleteProfile(): void {
    // Supprimer le profil de l'utilisateur (à implémenter)
    console.log('Profil supprimé');
  }
}
