export class User {
    _id: string = '';
    name: string = '';
    email: string = '';
    password: string = '';
    role: string = '';
    createdAt: Date = new Date();
    updatedAt: Date = new Date();
}