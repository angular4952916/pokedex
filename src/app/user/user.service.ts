import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { jwtDecode } from "jwt-decode";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  apiUrl = 'http://localhost:3000/api';



  getUserInfoFromToken(): Observable<any> {
    console.log("Let's get UserInfo from token:");
    
    // Récupérer le token depuis le localStorage
    const token = localStorage.getItem('token')||'';

    console.log('token: ' + token);
    

    // Extraire l'ID de l'utilisateur à partir du token
    const decodedToken: any = jwtDecode(token);
    const userId = decodedToken.userId;

    // Effectuer une requête vers le backend pour récupérer les informations de l'utilisateur
    return this.http.get<any>(`${this.apiUrl}/users/${userId}`);
  }
}
