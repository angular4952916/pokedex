import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css'
  ]
})
export class LoginComponent {

  loginForm: FormGroup = new FormGroup({
    'email': new FormControl(null, Validators.required),
    'password': new FormControl(null, Validators.required),
  })

  message: string|undefined;
  isMessageError: boolean = false;


  constructor(public authService: AuthService, private router: Router) {}
  
  login() {
    this.isMessageError = false;
    this.message = 'Tentative de connexion en cours ...';
  
    const email = this.loginForm.get('email');
    const password = this.loginForm.get('password');
  
    if (email?.value && password?.value) {
      this.authService.login(email.value, password.value)
        .pipe(
          catchError((error) => {
            // Gestion de l'erreur ici
            this.isMessageError = true;
            console.error('Erreur lors de la connexion :', error);
            this.message = 'Une erreur est survenue lors de la connexion';
            // Retourner un observable avec une valeur par défaut pour permettre à l'observable de continuer à émettre
            return of(null);
          })
        )
        .subscribe((isLoggedIn) => {
          if (isLoggedIn) {
            this.router.navigate(['/pokemons']);
          } else {
            this.isMessageError = true;
            this.message = 'Email ou mot de passe incorrect';
            this.loginForm.reset();
          }
        });
    } else {
      this.isMessageError = true;
      this.message = 'Les champs sont obligatoires';
    }
  }
  

goToRegisterPage(): void {
  this.router.navigate(['/register'], { replaceUrl: true });
}

}
