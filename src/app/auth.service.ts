import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, catchError, delay, of, tap } from 'rxjs';
import { User } from './user/user.models';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn: boolean = false;
  loginUrl = 'http://localhost:3000/api/users/login';
  registerUrl = 'http://localhost:3000/api/users';


  constructor(private router: Router, private httpClient: HttpClient) { }

  register(name: string, email: string, password: string, role: string): Observable<any> {

    console.log('Try to register...');
    
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }

    return this.httpClient.post<User>(this.registerUrl, {name, email, password, role}, httpOptions).pipe(
      tap(res => {
        console.log('Response after register: ', res);        
        this.router.navigate(['/login']);
      }),
      catchError( error => {
        console.log('Register error: ', error);
        return error;
       }
      )
    );
  }

  login(email: string, password: string): Observable<any> {

    console.log('Try to login...');
    
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }

    return this.httpClient.post<{token: string}>(this.loginUrl, {email, password}, httpOptions).pipe(
      tap(res => {
        console.log('token', res.token);
        
        localStorage.setItem('token', res.token);
        this.isLoggedIn = true;
        this.router.navigate(['/pokemons']);
      }),
      catchError( error => {
        console.log('Login error: ', error);
        return error;
       }
      )
    );
  }

  getToken(){
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.isLoggedIn = false;
    this.router.navigate(['/login']);
  }
}
