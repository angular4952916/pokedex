import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.css'
  ]
})
export class RegisterComponent {

  registerForm: FormGroup = new FormGroup({
    'name': new FormControl(null, Validators.required),
    'email': new FormControl(null, Validators.required),
    'password': new FormControl(null, Validators.required),
  })

  message: string|undefined;
  isMessageError: boolean = false;
  role: string = 'user';

  constructor(public authService: AuthService, private router: Router) {}

 
  
  register() {
    this.isMessageError = false;
    this.message = 'Tentative d\'inscription en cours ...';
  
    const name = this.registerForm.get('name');
    const email = this.registerForm.get('email');
    const password = this.registerForm.get('password');
  
    if (name?.value && email?.value && password?.value) {
      this.authService.register(name.value, email.value, password.value, this.role)
        .pipe(
          catchError((error) => {
            // Gestion de l'erreur ici
            this.isMessageError = true;
            console.error('Erreur lors de l\'inscription :', error.message);
            // this.message = error;
            this.message = 'Une erreur est survenue lors de l\'inscription';
            // Retourner un observable avec une valeur par défaut pour permettre à l'observable de continuer à émettre
            return of(null);
          })
        )
        .subscribe((isRegisteredIn) => {
          if (isRegisteredIn) {
            this.isMessageError = false;
            this.message = 'Inscription réussie !';
            this.router.navigate(['/login']);
          }
        });
    } else {
      this.isMessageError = true;
      this.message = 'Les champs sont obligatoires';
    }
  }
  

goToLoginPage(): void {
  this.router.navigate(['/login'], { replaceUrl: true });
}

}
